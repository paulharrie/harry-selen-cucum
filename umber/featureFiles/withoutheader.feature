Feature: Without header data table
Scenario: Login to orange HRM without header
	Given user is on log in page
	When the user enters the below creds
	|username|password|
	|Admin|admin123|
	|Admin1|admin1333|
	|Admin3|admin13343|
	And click login button
	Then the user should be navigated to the home



