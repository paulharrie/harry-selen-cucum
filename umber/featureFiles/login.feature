@login
Feature: This is the login feature for Orange HRM
@positive
Scenario: Login to orange HRM with valid creds
	Given user is on login page
	When the user enters valid "Admin" and "admin123"
	And clicks login button
	Then the user should be navigated to the home page

@negative
Scenario: Login to orange HRM with invalid creds
	Given user is on login page
	When the user enters valid "Admin" and "admin123"
	And clicks login button
	Then the user should be navigated to the home page



