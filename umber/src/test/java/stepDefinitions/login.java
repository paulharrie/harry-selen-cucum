package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class login {
	WebDriver driver;
	@Given("user is on login page")
	public void user_is_on_login_page() {
		System.setProperty("webdriver.chrome.driver", "C:\\cwebdriver\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.get("https://opensource-demo.orangehrmlive.com/");
	}
	@When("the user enters valid {string} and {string}")
	public void the_user_enters_valid_and(String un, String pw) {
	    // Write code here that turns the phrase above into concrete actions
	  	driver.findElement(By.name("txtUsername")).sendKeys(un);
	  	driver.findElement(By.name("txtPassword")).sendKeys(pw);
	}
	@When("clicks login button")
	public void clicks_login_button() {
	  	driver.findElement(By.name("Submit")).click();

	}
	@Then("the user should be navigated to the home page")
	public void the_user_should_be_navigated_to_the_home_page() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//h1[text()='Dashboard']"));
		driver.close();
	}


}
