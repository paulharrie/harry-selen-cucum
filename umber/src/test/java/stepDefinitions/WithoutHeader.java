package stepDefinitions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class WithoutHeader {
	WebDriver driver;
	@Given("user is on log in page")
	public void user_is_on_log_in_page() {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "C:\\cwebdriver\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.get("https://opensource-demo.orangehrmlive.com/");
	}

	@When("the user enters the below creds")
	public void the_user_enters_the_below_creds(io.cucumber.datatable.DataTable dataTable) {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
	    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
	    // Double, Byte, Short, Long, BigInteger or BigDecimal.
	    //
	    // For other transformations you can register a DataTableType.
		/*
		List<String> creds= dataTable.asList();
		String un=creds.get(0);
		String pw=creds.get(1);
		driver.findElement(By.name("txtUsername")).sendKeys(un);
	  	driver.findElement(By.name("txtPassword")).sendKeys(pw);
	  	*/
		//List<Map<Object, Object>> hm=dataTable.asMaps(String.class,String.class);
		//Map<String, String> hm=dataTable.asMap(String.class,String.class);
		//System.out.println(hm);
		List<String> creds= dataTable.asList();
		System.out.println(creds);
	}
	@When("the user enters the {string} and {string}")
	public void the_user_enters_the_and(String string, String string2) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("txtUsername")).sendKeys(string);
	  	driver.findElement(By.name("txtPassword")).sendKeys(string2);
	}

	@When("click login button")
	public void click_login_button() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("Submit")).click();	}

	@Then("the user should be navigated to the home")
	public void the_user_should_be_navigated_to_the_home() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.close();
	}

}
