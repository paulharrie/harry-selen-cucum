package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class openGoogle {

	 WebDriver driver ;

@Given("The user(s) hits {string}")
public void the_user_hits_google_com(String site) throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	System.setProperty("webdriver.chrome.driver", "C:\\cwebdriver\\chromedriver.exe");
    driver = new ChromeDriver();
    driver.get(site);
    
}
@When("user searches {string}")
public void user_searches(String searchterm) throws Throwable{
    // Write code here that turns the phrase above into concrete actions
    driver.findElement(By.name("q")).sendKeys(searchterm);
}
@When("enters return key")
public void enters_return_key() throws Throwable{
    // Write code here that turns the phrase above into concrete actions
	 driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
}
@Then("the user sees the search results")
public void the_user_sees_the_search_results() throws Throwable{
    // Write code here that turns the phrase above into concrete actions
    boolean flag = driver.findElement(By.partialLinkText("Harry")).isDisplayed();
    if(flag) {
    	
    	System.out.println("Success, as result was shown");
    }
   driver.close(); 
}

	
}
