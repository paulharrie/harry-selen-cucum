package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="featureFiles", 
		glue={"stepDefinitions","hooks"},
		dryRun = false,
		stepNotifications = true,
		monochrome = true,
		plugin = {"html:report/webreport.html","json:report/jsonreport.json","junit:report/xmlreport.xml"},
		tags = "@google")
public class runnerClass {

}
